################################################################
# MODifieR Workflow                                            #
################################################################

rm(list = ls())
library(foreach)
library(parallel)
library(doParallel)
library(MODifieR)
set.seed(777)

################################################################
# Method 1: MCODE                                              #
################################################################

## MCODE (clique-based algorithm to identify disease modules from DEGs)

# Identification of modules with mod_mcode function
# Input: HGNC Symbols for genes (object)
mcode_df <- matrix_J
mcode_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(mcode_df)-1, .packages = "MODifieR") %do% {
  mcode_name <- colnames(mcode_df[i])
  #print(mcode_name)
  mcode_module_df <- data.frame(GS = mcode_df$HGNC_Symbol, P.Val = (-log10(mcode_df[, i])), stringsAsFactors = FALSE)
  mcode_module_df <- mcode_module_df[order(mcode_module_df$P.Val, decreasing = TRUE), ]
  mcode_module_sum <- mod_mcode(deg_genes = mcode_module_df, type = "Gene symbol", 
                                hierarchy      = 1,      # Hierarchical level of the PPI network to be used
                                vwp            = 0.5,    # Vertex Weight Percentage
                                haircut        = FALSE,  # Boolean, if TRUE removes singly connected nodes from clusters
                                fluff          = FALSE,  # Boolean, if TRUE expands cluster cores by one neighbour shell outwards
                                fdt            = 0.8,    # Cluster density cutoff
                                loops          = TRUE,   # Boolean, if TRUE includes self-loops
                                diffgen_cutoff = 1.3,    # Threshold (expression) for genes to be considered DEGs
                                module_cutoff  = 3.5)    # Threshold (score) for output modules
  mcode_module_genes[[i]] <- mcode_module_sum[[1]]$module_genes
  names(mcode_module_genes)[i] <- mcode_name
  write.table(mcode_module_genes[i], paste0("~/Documents/R/Master_Thesis/GSE26/mcode_results/", mcode_name, ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)
  print(paste(mcode_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
mcode_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/mcode_results/")
mcode_module_genes <- list()
for (i in 1:length(mcode_results)) {
  mcode_results2 <- as.data.frame(read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/mcode_results/", mcode_results[i], sep = "")))
  mcode_module_genes[[i]] <- as.character(mcode_results2[, 1])
  names(mcode_module_genes)[i] <- mcode_results[i]
}

mcode_gene_list <- unique(unlist(mcode_module_genes)) 
adjmat_mcode <- matrix(0, nrow = length(mcode_gene_list), ncol = length(names(mcode_module_genes)))
rownames(adjmat_mcode) <- mcode_gene_list
colnames(adjmat_mcode) <- names(mcode_module_genes)
adjmat_mcode <- as.data.frame(adjmat_mcode)

for(i in 1:ncol(adjmat_mcode)) {
  print(i)
  x <- unlist(mcode_module_genes[[i]])
  adjmat_mcode[, i][rownames(adjmat_mcode) %in% x] <- 1
}


################################################################
# Method 2: Clique Sum                                         #
################################################################

## Clique Sum (clique-based algorithm by Fredrik Barrenäs to identify disease modules from DEGs using a core sum approach)

# Identification of modules with clique_sum function
# Input: HGNC Symbols for genes (object) and network (path)
cliquesum_df <- matrix_J
cliquesum_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(cliquesum_df)-1, .packages = "MODifieR") %do% {
  cliquesum_name <- colnames(cliquesum_df[i])
  #print(cliquesum_name)
  cliquesum_module_df <- data.frame(GS = cliquesum_df$HGNC_Symbol, P.Val = (-log10(cliquesum_df[, i])), stringsAsFactors = FALSE)
  cliquesum_module_df <- cliquesum_module_df[order(cliquesum_module_df$P.Val, decreasing = TRUE), ]
  cliquesum_module_sum <- clique_sum(deg_genes = cliquesum_module_df, ppi_network = "/home/dme/Documents/R/Master_Thesis/GSE26/MODifieR_Networks/HGNC_PPi_900.txt", 
                                     simplify_graph  = TRUE,   # Boolean, if TRUE simplifies the graph
                                     n_iter          = 10000,  # Number of iterations to be performed
                                     cutoff          = 0.05,   # Threshold (p-value)
                                     min_clique_size = 5,      # Minimal clique size
                                     min_deg_clique  = 3)      # Minimal degree of the clique
  cliquesum_module_genes[[i]] <- cliquesum_module_sum$module_genes
  names(cliquesum_module_genes)[i] <- cliquesum_name
  print(paste(cliquesum_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
cliquesum_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/cliquesum_results/")
cliquesum_module_genes <- list()
for (i in 1:length(cliquesum_results)) {
  cliquesum_results2 <- read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/cliquesum_results/", cliquesum_results[i], sep = ""))
  cliquesum_module_genes[[i]] <- as.character(cliquesum_results2[, 1])
  names(cliquesum_module_genes)[i] <- cliquesum_results[i]
}

cliquesum_gene_list <- unique(unlist(cliquesum_module_genes))  
adjmat_cliquesum <- matrix(0, nrow = length(cliquesum_gene_list), ncol = length(names(cliquesum_module_genes)))
rownames(adjmat_cliquesum) <- cliquesum_gene_list
colnames(adjmat_cliquesum) <- names(cliquesum_module_genes)
adjmat_cliquesum <- as.data.frame(adjmat_cliquesum)

for(i in 1:ncol(adjmat_cliquesum)) {
  print(i)
  x <- unlist(cliquesum_module_genes[[i]])
  adjmat_cliquesum[, i][rownames(adjmat_cliquesum) %in% x] <- 1
}


################################################################
# Method 3: Correlation Clique                                 #
################################################################

## Correlation Clique (clique-based algorithm to identify disease modules from correlated gene expression)

# Identification of modules with correlation_clique function
# Input: Entrez IDs for genes (object) and
corclique_df <- matrix_K
corclique_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(corclique_df)-1, .packages = "MODifieR") %do% {
  corclique_name <- colnames(corclique_df[i])
  #print(corclique_name)
  corclique_module_df <- data.frame(GS = corclique_df$Entrez_ID, P.Val = (-log10(corclique_df[, i])), stringsAsFactors = FALSE)
  corclique_module_df <- corclique_module_df[order(corclique_module_df$P.Val, decreasing = TRUE), ]
  corclique_module_sum <- correlation_clique(pval_matrix = , correlation_matrix = , ppi_network = ,
                                             frequency_cutoff       = 0.5,           # Threshold for significance out of number of iterations performed
                                             cutOffRank             = 700,           # Threshold (score) of the PPI network
                                             probabilityScaleFactor = 0.6,           # Scale for enriched cliques
                                             perturbation           = 10,            # Number of iterations to be performed
                                             cliqueTestCritera      = "FisherTest")  # Statistical test to be performed for enrichment of cliques
  corclique_module_genes[[i]] <- corclique_module_sum$module_genes
  names(corclique_module_genes)[i] <- corclique_name
  print(paste(corclique_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
corclique_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/corclique_results/")
corclique_module_genes <- list()
for (i in 1:length(corclique_results)) {
  corclique_results2 <- read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/corclique_results/", corclique_results[i], sep = ""))
  corclique_module_genes[[i]] <- as.character(corclique_results2[, 1])
  names(corclique_module_genes)[i] <- corclique_results[i]
}

corclique_gene_list <- unique(unlist(corclique_module_genes))  
adjmat_corclique <- matrix(0, nrow = length(corclique_gene_list), ncol = length(names(corclique_module_genes)))
rownames(adjmat_corclique) <- corclique_gene_list
colnames(adjmat_corclique) <- names(corclique_module_genes)
adjmat_corclique <- as.data.frame(adjmat_corclique)

for(i in 1:ncol(adjmat_corclique)) {
  print(i)
  x <- unlist(corclique_module_genes[[i]])
  adjmat_corclique[, i][rownames(adjmat_corclique) %in% x] <- 1
}


################################################################
# Method 4: DIAMOnD                                            #
################################################################

## DIAMOnD (seed-gene-based algorithm to identify disease modules from DEGs)

# Identification of modules with diamond function
# Input: Entrez IDs for genes (path) and network (path)
diamond_df <- matrix_K
diamond_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(diamond_df)-1, .packages = "MODifieR") %do% {
  diamond_name <- colnames(diamond_df[i])
  #print(diamond_name)
  diamond_module_df <- data.frame(GS = diamond_df$Entrez_ID, P.Val = (-log10(diamond_df[, i])), stringsAsFactors = FALSE)
  diamond_module_df <- diamond_module_df[order(diamond_module_df$P.Val, decreasing = TRUE), ]
  write.table(diamond_module_df[1:200, 1], file = "diamond_module_df.txt", col.names = FALSE, sep = "\t", dec = ".", row.names = FALSE, quote = FALSE)
  diamond_module_sum <- diamond(network = "/home/dme/Documents/R/Master_Thesis/GSE26/MODifieR_Networks/Entrez_PPi.txt", 
                                input_genes = "/home/dme/Documents/R/Master_Thesis/GSE26/diamond_module_df.txt", 
                                n_output_genes = 200,   # Maximum number of genes to include in the final module
                                seed_weight    = 1,     # Weight assignation for seed genes
                                include_seed   = TRUE)  # Boolean, if TRUE includes seed genes in the final module
  diamond_module_genes[[i]] <- diamond_module_sum$module_genes
  names(diamond_module_genes)[i] <- diamond_name
  write.table(diamond_module_genes[i], paste0("~/Documents/R/Master_Thesis/GSE26/diamond_results/", diamond_name, ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)
  print(paste(diamond_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
diamond_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/diamond_results/")
diamond_module_genes <- list()
for (i in 1:length(diamond_results)) {
  diamond_results2 <- read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/diamond_results/", diamond_results[i], sep = ""))
  diamond_module_genes[[i]] <- as.character(diamond_results2[, 1])
  names(diamond_module_genes)[i] <- diamond_results[i]
}

diamond_gene_list <- unique(unlist(diamond_module_genes))  
adjmat_diamond <- matrix(0, nrow = length(diamond_gene_list), ncol = length(names(diamond_module_genes)))
rownames(adjmat_diamond) <- diamond_gene_list
colnames(adjmat_diamond) <- names(diamond_module_genes)
adjmat_diamond <- as.data.frame(adjmat_diamond)

for(i in 1:ncol(adjmat_diamond)) {
  print(i)
  x <- unlist(diamond_module_genes[[i]])
  adjmat_diamond[, i][rownames(adjmat_diamond) %in% x] <- 1
}


################################################################
# Method 5: DiffCoEx                                           #
################################################################

## DiffCoEx (co-expression-based algorithm to identify disease modules)

# Identification of modules with diffcoex function
# Input:
diffcoex_df <- 
diffcoex_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(diffcoex_df)-1, .packages = "MODifieR") %do% {
  diffcoex_name <- colnames(diffcoex_df[i])
  #print(diffcoex_name)
  diffcoex_module_df <- data.frame(GS = diffcoex_df$HGNC_Symbol, P.Val = (-log10(diffcoex_df[, i])), stringsAsFactors = FALSE)
  diffcoex_module_df <- diffcoex_module_df[order(diffcoex_module_df$P.Val, decreasing = TRUE), ]
  diffcoex_module_sum <- diffcoex(dataset1 = , dataset2 = , 
                                  value                    = 20,          # 
                                  beta                     = 6,           # 
                                  cor_method               = "spearman",  # Correlation method to be used
                                  cluster_method           = "average",   # Cluster method to be used
                                  cuttree_method           = "hybrid",    #
                                  cut_height               = 0.996,       # 
                                  deep_split               = 0,           # 
                                  pam_respects             = FALSE,       # 
                                  min_cluster_size         = 20,          # 
                                  cluster_merge_cut_height = 0.2)         # 
  diffcoex_module_genes[[i]] <- diffcoex_module_sum$module_genes
  names(diffcoex_module_genes)[i] <- diffcoex_name
  print(paste(diffcoex_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
diffcoex_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/diffcoex_results/")
diffcoex_module_genes <- list()
for (i in 1:length(diffcoex_results)) {
  diffcoex_results2 <- read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/diffcoex_results/", diffcoex_results[i], sep = ""))
  diffcoex_module_genes[[i]] <- as.character(diffcoex_results2[, 1])
  names(diffcoex_module_genes)[i] <- diffcoex_results[i]
}

diffcoex_gene_list <- unique(unlist(diffcoex_module_genes))  
adjmat_diffcoex <- matrix(0, nrow = length(diffcoex_gene_list), ncol = length(names(diffcoex_module_genes)))
rownames(adjmat_diffcoex) <- diffcoex_gene_list
colnames(adjmat_diffcoex) <- names(diffcoex_module_genes)
adjmat_diffcoex <- as.data.frame(adjmat_diffcoex)

for(i in 1:ncol(adjmat_diffcoex)) {
  print(i)
  x <- unlist(diffcoex_module_genes[[i]])
  adjmat_diffcoex[, i][rownames(adjmat_diffcoex) %in% x] <- 1
}


################################################################
# Method 6: MODA                                               #
################################################################

## MODA (co-expression-based algorithm to identify disease modules)

# Identification of modules with moda function
# Input: 
moda_df <- 
moda_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(moda_df)-1, .packages = "MODifieR") %do% {
  moda_name <- colnames(moda_df[i])
  #print(moda_name)
  moda_module_df <- data.frame(GS = moda_df$HGNC_Symbol, P.Val = (-log10(moda_df[, i])), stringsAsFactors = FALSE)
  moda_module_df <- moda_module_df[order(moda_module_df$P.Val, decreasing = TRUE), ]
  moda_module_genes <- moda(datExpr1 = , datExpr2 = , result_folder = , 
                            CuttingCriterion   = "Density",  # 
                            indicator_datExpr1 = "control",  # 
                            indicator_datExpr2 = "patient",  # 
                            specificTheta      = 0.1,        # 
                            conservedTheta     = 0.1)        # 
  moda_module_genes[[i]] <- moda_module_sum$module_genes
  names(moda_module_genes)[i] <- moda_name
  print(paste(moda_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
moda_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/moda_results/")
moda_module_genes <- list()
for (i in 1:length(moda_results)) {
  moda_results2 <- read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/moda_results/", moda_results[i], sep = ""))
  moda_module_genes[[i]] <- as.character(moda_results2[, 1])
  names(moda_module_genes)[i] <- moda_results[i]
}

moda_gene_list <- unique(unlist(moda_module_genes))  
adjmat_moda <- matrix(0, nrow = length(moda_gene_list), ncol = length(names(moda_module_genes)))
rownames(adjmat_moda) <- moda_gene_list
colnames(adjmat_moda) <- names(moda_module_genes)
adjmat_moda <- as.data.frame(adjmat_moda)

for(i in 1:ncol(adjmat_moda)) {
  print(i)
  x <- unlist(moda_module_genes[[i]])
  adjmat_moda[, i][rownames(adjmat_moda) %in% x] <- 1
}


################################################################
# Method 7: Module Discoverer                                  #
################################################################

## Module Discoverer (clique-based algorithm by Sebastian Vlaic to identify disease modules from DEGs)

# Identification of modules with modulediscoverer function
# Input: HGNC Symbols for genes (object) and Ensembl IDs for network (object)
discoverer_df <- matrix_J
discoverer_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(discoverer_df)-1, .packages = "MODifieR") %do% {
  discoverer_name <- colnames(discoverer_df[i])
  #print(discoverer_name)
  discoverer_module_df <- data.frame(GS = discoverer_df$HGNC_Symbol, P.Val = (-log10(discoverer_df[, i])), stringsAsFactors = FALSE)
  discoverer_module_df <- discoverer_module_df[order(discoverer_module_df$P.Val, decreasing = TRUE), ]
  discoverer_module_sum <- modulediscoverer(diffgen = discoverer_module_df, ppi_network = ENSp_ppi_network, 
                                            permutations = 1000,  # Number of permutations
                                            pval_cutoff  = 1.3,   # Threshold for p-values for choosing number of DEGs
                                            repeats      = 1.5,   # Number of repeats to be performed per single-seed run
                                            times        = 1000,  # Number of iterations to be performed
                                            p_val        = 0.05)  # Threshold (p-value) for significant cliques
  discoverer_module_genes[[i]] <- discoverer_module_sum$module_genes
  names(discoverer_module_genes)[i] <- discoverer_name
  print(paste(discoverer_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
discoverer_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/discoverer_results/")
discoverer_module_genes <- list()
for (i in 1:length(discoverer_results)) {
  discoverer_results2 <- read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/discoverer_results/", discoverer_results[i], sep = ""))
  discoverer_module_genes[[i]] <- as.character(discoverer_results2[, 1])
  names(discoverer_module_genes)[i] <- discoverer_results[i]
}

discoverer_gene_list <- unique(unlist(discoverer_module_genes))  
adjmat_discoverer <- matrix(0, nrow = length(discoverer_gene_list), ncol = length(names(discoverer_module_genes)))
rownames(adjmat_discoverer) <- discoverer_gene_list
colnames(adjmat_discoverer) <- names(discoverer_module_genes)
adjmat_discoverer <- as.data.frame(adjmat_discoverer)

for(i in 1:ncol(adjmat_discoverer)) {
  print(i)
  x <- unlist(discoverer_module_genes[[i]])
  adjmat_discoverer[, i][rownames(adjmat_discoverer) %in% x] <- 1
}


################################################################
# Method 8: WGCNA                                              #
################################################################

## WGCNA (co-expression-based algorithm to identify disease modules)

# Identification of modules with wgcna function
# Input:
wgcna_df <- 
wgcna_module_genes <- list()
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(wgcna_df)-1, .packages = "MODifieR") %do% {
  wgcna_name <- colnames(wgcna_df[i])
  #print(wgcna_name)
  wgcna_module_df <- data.frame(GS = wgcna_df$HGNC_Symbol, P.Val = (-log10(wgcna_df[, i])), stringsAsFactors = FALSE)
  wgcna_module_df <- wgcna_module_df[order(wgcna_module_df$P.Val, decreasing = TRUE), ]
  wgcna_module_sum <- wgcna(dataset1 = , dataset2 = , traits_file = , setLabels = , geofile = ,
                            pval_cutoff         = ,       #
                            min_module_size     = 30,     #
                            deep_split          = 2,      #
                            pam_respects_dendro = FALSE,  #
                            merge_cut_height    = 0.1,    #
                            numeric_labels      = TRUE,   #
                            min_KME             = 0,      #
                            save_toms           = TRUE)   #
  wgcna_module_genes[[i]] <- wgcna_module_sum$module_genes
  names(wgcna_module_genes)[i] <- wgcna_name
  print(paste(wgcna_name, "module has been created", sep = " "))
}

# Design of the adjacency matrix
wgcna_results <- list.files(path = "/home/dme/Documents/R/Master_Thesis/GSE26/wgcna_results/")
wgcna_module_genes <- list()
for (i in 1:length(wgcna_results)) {
  wgcna_results2 <- read.csv(paste("/home/dme/Documents/R/Master_Thesis/GSE26/wgcna_results/", wgcna_results[i], sep = ""))
  wgcna_module_genes[[i]] <- as.character(wgcna_results2[, 1])
  names(wgcna_module_genes)[i] <- wgcna_results[i]
}

wgcna_gene_list <- unique(unlist(wgcna_module_genes))  
adjmat_wgcna <- matrix(0, nrow = length(wgcna_gene_list), ncol = length(names(wgcna_module_genes)))
rownames(adjmat_wgcna) <- wgcna_gene_list
colnames(adjmat_wgcna) <- names(wgcna_module_genes)
adjmat_wgcna <- as.data.frame(adjmat_wgcna)

for(i in 1:ncol(adjmat_wgcna)) {
  print(i)
  x <- unlist(wgcna_module_genes[[i]])
  adjmat_wgcna[, i][rownames(adjmat_wgcna) %in% x] <- 1
}


################################################################
# Robust Module Consensus                                      #
################################################################

## Robust Module Consensus (combination of modules identified by the previous methods to produce intersected modules)

# Combination of modules with super_module function
super_module(module_list = , 
             include_single_methods = TRUE)  # Boolean, if TRUE includes single methods

