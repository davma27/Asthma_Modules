################################################################
# INPUT 1: data frame of HGNC symbols/samples of p-values      #
################################################################

library(limma)
library(GEOquery)
library(oligo)
library(foreach)
library(parallel)
library(doParallel)

# Obtain Affy IDs/samples matrix of p-values
gset <- getGEO("GSE69683")
ex <- exprs(gset$GSE69683_series_matrix.txt.gz)
matrix_A <- matrix(data = 0, nrow = ncol(eset), ncol = ncol(eset))
diag(matrix_A) <- 1
matrix_B <- matrix(data = NA, nrow = nrow(eset), ncol = ncol(eset))
rownames(matrix_B) <- rownames(gset)
colnames(matrix_B) <- colnames(gset)

cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
system.time(foreach(i = 1:nrow(matrix_A), .packages = "limma") %dopar% {
  design <- model.matrix(~0 + as.character(matrix_A[, i]), as.data.frame(matrix_A))
  colnames(design) <- c("A", "B")
  fit <- lmFit(eset, design)
  contrast.matrix <- makeContrasts(test = B-A, levels = design)
  fit2 <- contrasts.fit(fit, contrast.matrix)
  fit2 <- eBayes(fit2)
  probe_1_sig <- topTable(fit2, n = Inf, coef = 1, adjust = "BH")
  probe_1_sig <- probe_1_sig[match(rownames(matrix_B), rownames(probe_1_sig)), ]
  matrix_B[, i] <- probe_1_sig$P.Value
  print(paste(i, "is added to matrix_B", sep = " "))
})
stopCluster(cl)

# Extract GPL (array platform) info
source("https://bioconductor.org/biocLite.R")
biocLite("GEOquery")
library(GEOquery)
gpl <- getGEO("GPL13158", destdir = ".")
Meta(gpl)$title
annot_info <- Table(gpl)[, c(1, 11, 12)] # 16 levels/annotation formats

# Add HGNC symbols to Affy IDs/samples matrix of p-values
matrix_C <- matrix(data = NA, nrow = nrow(eset), ncol = (ncol(eset) + 1))
rownames(matrix_C) <- annot_info[, 2]
matrix_C[, 1] <- rownames(matrix_B)
colnames(matrix_C)[2:499] <- colnames(matrix_B)
colnames(matrix_C)[1] <- "Affy_ID"
matrix_C <- as.data.frame(matrix_C)
matrix_C[, 1] <- as.character(matrix_C[, 1])
matrix_C[, 2:499] <- as.numeric(as.matrix(round(matrix_B[, 1:498], digits = 8)))

# Remove incomplete cases for HGNC symbols
matrix_D <- matrix_C
rownames(matrix_D) <- matrix_C[, 1]
matrix_D[, 1] <- rownames(matrix_C)
colnames(matrix_D)[1] <- "HGNC_Symbol"
is.na(matrix_D[, 1]) <- which(matrix_D[, 1] == "") 
matrix_D <- matrix_D[complete.cases(matrix_D[, 1]), ]

# Combine probes related to the same gene (keeping the lowest p-value for each sample), conserving the order
library(plyr)
keeping.order <- function(data, fn, ...) { 
  col <- ".sortColumn"
  data[, col] <- 1:nrow(data) 
  out <- fn(data, ...) 
  if (!col %in% colnames(out)) stop("Ordering column not preserved by function") 
  out <- out[order(out[, col]), ] 
  out[, col] <- NULL 
  out 
}

matrix_E <- keeping.order(matrix_D, ddply, .(matrix_D$HGNC_Symbol), numcolwise(min))
rownames(matrix_E) <- matrix_E[, 1]
matrix_E[1] <- NULL

# Duplication and expansion of input rows with 2+ HGNC Gene Symbols (separated by ///)
matrix_H <- matrix_E
matrix_H$HGNC_Symbol <- rownames(matrix_H)
interesting_fields <- c("HGNC_Symbol", colnames(matrix_E))
matrix_H <- matrix_H[interesting_fields]
to_be_duplicated <- c(colnames(matrix_E))
to_be_expanded <- c("HGNC_Symbol")

# Splitting conditions
matrix_split1 <- strsplit(matrix_H$HGNC_Symbol, split = " /// ")
#matrix_split2 <- strsplit(matrix_H$X, split = " /// ")

# Duplication of rows
matrix_temp <- matrix_H[, to_be_duplicated]
matrix_I <- matrix_temp[rep(seq_len(nrow(matrix_temp)), sapply(matrix_split1, length)), ]

# Expansion of rows (same p-value)
matrix_I[, to_be_expanded[1]] <- unlist(matrix_split1)
#matrix_I[, to_be_expanded[2]] <- unlist(matrix_split2)

# Output
matrix_J <- unique(matrix_I)
write.table(matrix_J, file = "matrix_J.txt", row.names = TRUE, col.names = TRUE, sep = ",", dec = ".")


################################################################
#INPUT 2: data frames of Entrez IDs/samples, ranked by p-value #
################################################################

matrix_F <- matrix_E
annot_unique <- annot_info[annot_info[, 2] %in% rownames(matrix_F), ]
annot_unique <- annot_unique[!duplicated(annot_unique[, 2]), ]
matrix_F <- as.matrix(matrix_F)
rownames(matrix_F) <- annot_unique[, 3]
matrix_F <- as.data.frame(matrix_F)

matrix_G <- matrix(data = NA, nrow = nrow(matrix_F), ncol = 2)
colnames(matrix_G) <- c("Entrez_ID", "P.Val")
matrix_G <- as.data.frame(matrix_G)
matrix_G[, 1] <- as.character(matrix_G[, 1])
matrix_G[, 2] <- as.numeric(matrix_G[, 2])

# Obtain Entrez IDs/samples matrix of p-values
cl = makeCluster(detectCores()-2)
registerDoParallel(cl)
foreach(i = 1:ncol(matrix_F)) %dopar% {
  matrix_ <- matrix_G
  matrix_[, 1] <- rownames(matrix_F)
  matrix_[, 2] <- matrix_F[, i]
  matrix_ <- matrix_[order(matrix_$P.Val, decreasing = FALSE), ]
  assign(paste("matrix_", colnames(matrix_F)[i], sep = ""), matrix_)
  print(paste("matrix_", colnames(matrix_F)[i], " has been created", sep = ""))
  }
stopCluster(cl)


################################################################
#INPUT 3: data frame of Entrez IDs/samples of p-values         #
################################################################

# Duplication and expansion of input rows with 2+ Entrez IDs (separated by ///)
matrix_K <- matrix_F
matrix_K$Entrez_ID <- rownames(matrix_K)
interesting_fields <- c("Entrez_ID", colnames(matrix_F))
matrix_K <- matrix_K[interesting_fields]
to_be_duplicated <- c(colnames(matrix_F))
to_be_expanded <- c("Entrez_ID")

# Splitting conditions
matrix_split1 <- strsplit(matrix_K$Entrez_ID, split = " /// ")
#matrix_split2 <- strsplit(matrix_K$X, split = " /// ")

# Duplication of rows
matrix_temp <- matrix_K[, to_be_duplicated]
matrix_K <- matrix_temp[rep(seq_len(nrow(matrix_temp)), sapply(matrix_split1, length)), ]

# Expansion of rows (same p-value)
matrix_K[, to_be_expanded[1]] <- unlist(matrix_split1)
#matrix_K[, to_be_expanded[2]] <- unlist(matrix_split2)

# Output
matrix_K <- unique(matrix_K)
write.table(matrix_K, file = "matrix_K.txt", row.names = TRUE, col.names = TRUE, sep = ",", dec = ".")


################################################################
# Result interpretation                                        #
################################################################

# matrix_A: 498x498 matrix with diagonal 1s, required for the design matrix prior to matrix_B
# matrix_B: 54715x498 matrix of Affy IDs/Samples, with p-values
# matrix_C: 54715x499 data frame of HGNC Symbols/Samples, with p-values (+ Affy IDs)
# matrix_D: 41796x499 data frame of Affy IDs/Samples, with p-values (+ HGNC Symbols, only complete cases)
# matrix_E: 20741x498 data frame of HGNC Symbols/Samples, with p-values (repeated HGNC Symbols combined for the minimal p-value)
# matrix_F: 20741x498 data frame of Entrez IDs/Samples, with p-values
# matrix_G: 20741x2 data frame, template for addition of Entrez IDs and p-values
# matrix_H: 20741x499 data frame of HGNC Symbols/Samples, with p-values (+ HGNC Symbols), template for duplication and expansion
# matrix_I: 22238x499 data frame of HGNC Symbols/Samples, with p-values (+ HGNC Symbols), output of row duplication and expansion
# matrix_J: 22238x499 data frame of HGNC Symbols/Samples, with p-values (+ HGNC Symbols), output of unique function [INPUT 1A]
# matrix_GSMXXXXXXX: 20741x2 data frames of Entrez IDs and p-values for a single sample (ranked by increasing p-value) [INPUT 2]
# matrix_K: 22232x499 data frame of Entrez IDs/Samples, with p-values (+ Entrez IDs), output of unique function [INPUT 1B]
