################################################################
# Method 2: Clique Sum (gamma)                                 #
################################################################

Args <- commandArgs(TRUE)
#print(Args[1])
#print(Args[2])
#print(Args[3])

## Clique Sum (clique-based algorithm by Fredrik Barrenäs to identify disease modules from DEGs using a core sum approach)
# Input: HGNC Symbols for genes (object) and network (path)
library(MODifieR, lib.loc = "/proj/lassim/disease_modules/rlibrary")
library(foreach)
library(parallel)
library(doParallel)
#setwd("/home/x_damar/Documents/R/Master_Thesis/GSE25")
matrix_J <- read.csv(paste(Args[1], sep = ""), sep = ",", dec = ".", row.names = 1, stringsAsFactors = FALSE)

# Identification of modules with clique_sum function
cliquesum_df <- matrix_J
cliquesum_module_genes <- list()
#cl = makeCluster(detectCores())
#registerDoParallel(cl)
#foreach(i = 1:ncol(cliquesum_df), .packages = "MODifieR") %do% {
cliquesum_name <- colnames(cliquesum_df[as.numeric(Args[3])])
#print(cliquesum_name)
cliquesum_module_df <- data.frame(GS = cliquesum_df$HGNC_Symbol, P.Val = (-log10(cliquesum_df[, as.numeric(Args[3])])), stringsAsFactors = FALSE)
rm(matrix_J)
rm(cliquesum_df)
cliquesum_module_df <- cliquesum_module_df[order(cliquesum_module_df$P.Val, decreasing = TRUE), ]
cliquesum_module_sum <- clique_sum(deg_genes = cliquesum_module_df, ppi_network = paste0(Args[2]), 
                                   simplify_graph  = TRUE,   # Boolean, if TRUE simplifies the graph
                                   n_iter          = 10000,  # Number of iterations to be performed
                                   cutoff          = 0.05,   # Threshold (p-value)
                                   min_clique_size = 5,      # Minimal clique size
                                   min_deg_clique  = 3)      # Minimal degree of the clique
cliquesum_module_genes <- cliquesum_module_sum$module_genes
write.table(cliquesum_module_genes, paste0("~/Documents/R/Master_Thesis/GSE25/cliquesum_results/", cliquesum_name, ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)
print(paste(cliquesum_name, "module has been created", sep = " "))
#}
