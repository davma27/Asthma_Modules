################################################################
# U-BIOPRED baseline phenotypic statistical analysis           #
################################################################

setwd("/home/dme/Desktop/Master/Master Thesis/Data")
metadata_G62 <- read.csv("endotype_list_updated.csv", header = TRUE, stringsAsFactors = FALSE)
numeric_columns <- c(5:18, 25:33) # Select data columns to be converted to numeric class
for (i in numeric_columns){
  metadata_G62[, i] <- (metadata_G62[, i] = as.numeric(metadata_G62[, i]))
}
metadata_G62 <- metadata_G62[complete.cases(metadata_G62$induced_sputum), ]
sum_G62 <- matrix(data = NA, nrow = 71, ncol = 4)
colnames(sum_G62) <- c("H", "MA", "SA", "p-value")
rownames(sum_G62) <- c("Sample_size", "Female", "Female_%", "Age", "Age_SD", "BMI", "BMI_SD", "Smokers", "Smokers_%", 
                       "n_Total_IgE_(IU/ml)", "Total_IgE_(IU/ml)", "Total_IgE_(IU/ml)_Q1", "Total_IgE_(IU/ml)_Q3", 
                       "n_CRP_(mg/l)", "CRP_(mg/l)", "CRP_(mg/l)_Q1", "CRP_(mg/l)_Q3", 
                       "n_induced_sputum_eosinophils_(10^3/ul)", "induced_sputum_eosinophils_(10^3/ul)", "induced_sputum_eosinophils_(10^3/ul)_Q1", "induced_sputum_eosinophils_(10^3/ul)_Q3", 
                       "n_induced_sputum_neutrophils_(10^3/ul)", "induced_sputum_neutrophils_(10^3/ul)", "induced_sputum_neutrophils_(10^3/ul)_Q1", "induced_sputum_neutrophils_(10^3/ul)_Q3", 
                       "n_Periostin_(ng/ml)", "Periostin_(ng/ml)", "Periostin_(ng/ml)_Q1", "Periostin_(ng/ml)_Q3", 
                       "n_Sputum_eosinophils_%", "Sputum_eosinophils_%", "Sputum_eosinophils_%_Q1", "Sputum_eosinophils_%_Q3", 
                       "n_Sputum_neutrophils_%", "Sputum_neutrophils_%", "Sputum_neutrophils_%_Q1", "Sputum_neutrophils_%_Q3", 
                       "n_Sputum_macrophages_%", "Sputum_macrophages_%", "Sputum_macrophages_%_Q1", "Sputum_macrophages_%_Q3", 
                       "n_Sputum_lymphocytes_%", "Sputum_lymphocytes_%", "Sputum_lymphocytes_%_Q1", "Sputum_lymphocytes_%_Q3", 
                       "n_Sputum_mast_cells_%", "Sputum_mast_cells_%", "Sputum_mast_cells_%_Q1", "Sputum_mast_cells_%_Q3", 
                       "n_FeNO_(ppb)", "FeNO_(ppb)", "FeNO_(ppb)_Q1", "FeNO_(ppb)_Q3", 
                       "n_FEV1_%", "FEV1_%", "FEV1_%_Q1", "FEV1_%_Q3", 
                       "Eczema", "Eczema_%", "Allergic_rhinitis", "Allergic_rhinitis_%", 
                       "Atopy", "Atopy_%", "Sinusitis", "Sinusitis_%", 
                       "Nasal_polyps", "Nasal_polyps_%", "Oral_cortiscosteroid_use", "Oral_corticosteroid_use_%", 
                       "White_caucasian", "White_caucasian_%")

# Sample size (SA: severe asthma, MA: moderate asthma, H: healthy)
metadata_G62$induced_sputum[grep("Healthy|healthy", metadata_G62$induced_sputum)] <- "Healthy"
G62_H <- subset(metadata_G62, induced_sputum == "Healthy")
sum_G62[1, 1] <- nrow(G62_H)

metadata_G62$induced_sputum[grep("Moderate|moderate", metadata_G62$induced_sputum)] <- "Moderate asthma"
G62_MA <- subset(metadata_G62, induced_sputum == "Moderate asthma")
sum_G62[1, 2] <- nrow(G62_MA)

metadata_G62$induced_sputum[grep("Severe|severe", metadata_G62$induced_sputum)] <- "Severe asthma"
G62_SA <- subset(metadata_G62, induced_sputum == "Severe asthma")
sum_G62[1, 3] <- nrow(G62_SA)

# Female
sum_G62[2, 1] <- nrow(subset(metadata_G62, Sex == "female" & induced_sputum == "Healthy"))
sum_G62[3, 1] <- round((sum_G62[2, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[2, 2] <- nrow(subset(metadata_G62, Sex == "female" & induced_sputum == "Moderate asthma"))
sum_G62[3, 2] <- round((sum_G62[2, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[2, 3] <- nrow(subset(metadata_G62, Sex == "female" & induced_sputum == "Severe asthma"))
sum_G62[3, 3] <- round((sum_G62[2, 3]/sum_G62[1, 3])*100, digits = 1)

# Age (y)
sum_G62[4, 1] <- round(mean(G62_H$Age), digits = 0)
sum_G62[5, 1] <- round(sd(G62_H$Age), digits = 0)

sum_G62[4, 2] <- round(mean(G62_MA$Age), digits = 0)
sum_G62[5, 2] <- round(sd(G62_MA$Age), digits = 0)

sum_G62[4, 3] <- round(mean(G62_SA$Age), digits = 0)
sum_G62[5, 3] <- round(sd(G62_SA$Age), digits = 0)

# BMI (kg/m2)
sum_G62[6, 1] <- round(mean(G62_H$Body_Mass_Index_.kg.m2.), digits = 1)
sum_G62[7, 1] <- round(sd(G62_H$Body_Mass_Index_.kg.m2.), digits = 1)

sum_G62[6, 2] <- round(mean(G62_MA$Body_Mass_Index_.kg.m2.), digits = 1)
sum_G62[7, 2] <- round(sd(G62_MA$Body_Mass_Index_.kg.m2.), digits = 1)

sum_G62[6, 3] <- round(mean(G62_SA$Body_Mass_Index_.kg.m2.), digits = 1)
sum_G62[7, 3] <- round(sd(G62_SA$Body_Mass_Index_.kg.m2.), digits = 1)

# Smokers
sum_G62[8, 1] <- 0
sum_G62[9, 1] <- 0

sum_G62[8, 2] <- 0
sum_G62[9, 2] <- 0

sum_G62[8, 3] <- 0
sum_G62[9, 3] <- 0

# Total IgE (IU/ml)
sum_G62[10, 1] <- round(nrow(G62_H[complete.cases(G62_H$IgE_Total_.IU.ml.), ] == TRUE), digits = 0)
sum_G62[11, 1] <- round(median(G62_H$IgE_Total_.IU.ml., na.rm = TRUE), digits = 1)
sum_G62[12, 1] <- round(quantile(G62_H$IgE_Total_.IU.ml., na.rm = TRUE, 0.25), digits = 1)
sum_G62[13, 1] <- round(quantile(G62_H$IgE_Total_.IU.ml., na.rm = TRUE, 0.75), digits = 1)

sum_G62[10, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$IgE_Total_.IU.ml.), ] == TRUE), digits = 0)
sum_G62[11, 2] <- round(median(G62_MA$IgE_Total_.IU.ml., na.rm = TRUE), digits = 1)
sum_G62[12, 2] <- round(quantile(G62_MA$IgE_Total_.IU.ml., na.rm = TRUE, 0.25), digits = 1)
sum_G62[13, 2] <- round(quantile(G62_MA$IgE_Total_.IU.ml., na.rm = TRUE, 0.75), digits = 1)

sum_G62[10, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$IgE_Total_.IU.ml.), ] == TRUE), digits = 0)
sum_G62[11, 3] <- round(median(G62_SA$IgE_Total_.IU.ml., na.rm = TRUE), digits = 1)
sum_G62[12, 3] <- round(quantile(G62_SA$IgE_Total_.IU.ml., na.rm = TRUE, 0.25), digits = 1)
sum_G62[13, 3] <- round(quantile(G62_SA$IgE_Total_.IU.ml., na.rm = TRUE, 0.75), digits = 1)

# C-reactive protein (mg/l)
sum_G62[14, 1] <- round(nrow(G62_H[complete.cases(G62_H$C_Reactive_Protein_.mg.L.), ] == TRUE), digits = 0)
sum_G62[15, 1] <- round(median(G62_H$C_Reactive_Protein_.mg.L., na.rm = TRUE), digits = 1)
sum_G62[16, 1] <- round(quantile(G62_H$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.25), digits = 1)
sum_G62[17, 1] <- round(quantile(G62_H$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.75), digits = 1)

sum_G62[14, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$C_Reactive_Protein_.mg.L.), ] == TRUE), digits = 0)
sum_G62[15, 2] <- round(median(G62_MA$C_Reactive_Protein_.mg.L., na.rm = TRUE), digits = 1)
sum_G62[16, 2] <- round(quantile(G62_MA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.25), digits = 1)
sum_G62[17, 2] <- round(quantile(G62_MA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.75), digits = 1)

sum_G62[14, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$C_Reactive_Protein_.mg.L.), ] == TRUE), digits = 0)
sum_G62[15, 3] <- round(median(G62_SA$C_Reactive_Protein_.mg.L., na.rm = TRUE), digits = 1)
sum_G62[16, 3] <- round(quantile(G62_SA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.25), digits = 1)
sum_G62[17, 3] <- round(quantile(G62_SA$C_Reactive_Protein_.mg.L., na.rm = TRUE, 0.75), digits = 1)

# induced_sputum eosinophils (10^3/ul)
sum_G62[18, 1] <- round(nrow(G62_H[complete.cases(G62_H$eosinophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G62[19, 1] <- round(median(G62_H$eosinophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G62[20, 1] <- round(quantile(G62_H$eosinophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G62[21, 1] <- round(quantile(G62_H$eosinophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G62[18, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$eosinophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G62[19, 2] <- round(median(G62_MA$eosinophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G62[20, 2] <- round(quantile(G62_MA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G62[21, 2] <- round(quantile(G62_MA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G62[18, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$eosinophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G62[19, 3] <- round(median(G62_SA$eosinophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G62[20, 3] <- round(quantile(G62_SA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G62[21, 3] <- round(quantile(G62_SA$eosinophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

# induced_sputum neutrophils (10^3/ul)
sum_G62[22, 1] <- round(nrow(G62_H[complete.cases(G62_H$neutrophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G62[23, 1] <- round(median(G62_H$neutrophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G62[24, 1] <- round(quantile(G62_H$neutrophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G62[25, 1] <- round(quantile(G62_H$neutrophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G62[22, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$neutrophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G62[23, 2] <- round(median(G62_MA$neutrophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G62[24, 2] <- round(quantile(G62_MA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G62[25, 2] <- round(quantile(G62_MA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

sum_G62[22, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$neutrophils_.x10.3.uL.), ] == TRUE), digits = 0)
sum_G62[23, 3] <- round(median(G62_SA$neutrophils_.x10.3.uL., na.rm = TRUE), digits = 2)
sum_G62[24, 3] <- round(quantile(G62_SA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.25), digits = 2)
sum_G62[25, 3] <- round(quantile(G62_SA$neutrophils_.x10.3.uL., na.rm = TRUE, 0.75), digits = 2)

# Periostin (ng/ml)
sum_G62[26, 1] <- round(nrow(G62_H[complete.cases(G62_H$Periostin_.ng.mL.), ] == TRUE), digits = 0)
sum_G62[27, 1] <- round(median(G62_H$Periostin_.ng.mL., na.rm = TRUE), digits = 1)
sum_G62[28, 1] <- round(quantile(G62_H$Periostin_.ng.mL., na.rm = TRUE, 0.25), digits = 1)
sum_G62[29, 1] <- round(quantile(G62_H$Periostin_.ng.mL., na.rm = TRUE, 0.75), digits = 1)

sum_G62[26, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$Periostin_.ng.mL.), ] == TRUE), digits = 0)
sum_G62[27, 2] <- round(median(G62_MA$Periostin_.ng.mL., na.rm = TRUE), digits = 1)
sum_G62[28, 2] <- round(quantile(G62_MA$Periostin_.ng.mL., na.rm = TRUE, 0.25), digits = 1)
sum_G62[29, 2] <- round(quantile(G62_MA$Periostin_.ng.mL., na.rm = TRUE, 0.75), digits = 1)

sum_G62[26, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$Periostin_.ng.mL.), ] == TRUE), digits = 0)
sum_G62[27, 3] <- round(median(G62_SA$Periostin_.ng.mL., na.rm = TRUE), digits = 1)
sum_G62[28, 3] <- round(quantile(G62_SA$Periostin_.ng.mL., na.rm = TRUE, 0.25), digits = 1)
sum_G62[29, 3] <- round(quantile(G62_SA$Periostin_.ng.mL., na.rm = TRUE, 0.75), digits = 1)

# Sputum eosinophils (%)
sum_G62[30, 1] <- round(nrow(G62_H[complete.cases(G62_H$Pct_Eosinophils), ] == TRUE), digits = 0)
sum_G62[31, 1] <- round(median(G62_H$Pct_Eosinophils, na.rm = TRUE), digits = 1)
sum_G62[32, 1] <- round(quantile(G62_H$Pct_Eosinophils, na.rm = TRUE, 0.25), digits = 1)
sum_G62[33, 1] <- round(quantile(G62_H$Pct_Eosinophils, na.rm = TRUE, 0.75), digits = 1)

sum_G62[30, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$Pct_Eosinophils), ] == TRUE), digits = 0)
sum_G62[31, 2] <- round(median(G62_MA$Pct_Eosinophils, na.rm = TRUE), digits = 1)
sum_G62[32, 2] <- round(quantile(G62_MA$Pct_Eosinophils, na.rm = TRUE, 0.25), digits = 1)
sum_G62[33, 2] <- round(quantile(G62_MA$Pct_Eosinophils, na.rm = TRUE, 0.75), digits = 1)

sum_G62[30, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$Pct_Eosinophils), ] == TRUE), digits = 0)
sum_G62[31, 3] <- round(median(G62_SA$Pct_Eosinophils, na.rm = TRUE), digits = 1)
sum_G62[32, 3] <- round(quantile(G62_SA$Pct_Eosinophils, na.rm = TRUE, 0.25), digits = 1)
sum_G62[33, 3] <- round(quantile(G62_SA$Pct_Eosinophils, na.rm = TRUE, 0.75), digits = 1)

# Sputum neutrophils (%)
sum_G62[34, 1] <- round(nrow(G62_H[complete.cases(G62_H$Pct_Neutrophils), ] == TRUE), digits = 0)
sum_G62[35, 1] <- round(median(G62_H$Pct_Neutrophils, na.rm = TRUE), digits = 1)
sum_G62[36, 1] <- round(quantile(G62_H$Pct_Neutrophils, na.rm = TRUE, 0.25), digits = 1)
sum_G62[37, 1] <- round(quantile(G62_H$Pct_Neutrophils, na.rm = TRUE, 0.75), digits = 1)

sum_G62[34, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$Pct_Neutrophils), ] == TRUE), digits = 0)
sum_G62[35, 2] <- round(median(G62_MA$Pct_Neutrophils, na.rm = TRUE), digits = 1)
sum_G62[36, 2] <- round(quantile(G62_MA$Pct_Neutrophils, na.rm = TRUE, 0.25), digits = 1)
sum_G62[37, 2] <- round(quantile(G62_MA$Pct_Neutrophils, na.rm = TRUE, 0.75), digits = 1)

sum_G62[34, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$Pct_Neutrophils), ] == TRUE), digits = 0)
sum_G62[35, 3] <- round(median(G62_SA$Pct_Neutrophils, na.rm = TRUE), digits = 1)
sum_G62[36, 3] <- round(quantile(G62_SA$Pct_Neutrophils, na.rm = TRUE, 0.25), digits = 1)
sum_G62[37, 3] <- round(quantile(G62_SA$Pct_Neutrophils, na.rm = TRUE, 0.75), digits = 1)

# Sputum macrophages (%)
sum_G62[38, 1] <- round(nrow(G62_H[complete.cases(G62_H$Pct_Macrophages), ] == TRUE), digits = 0)
sum_G62[39, 1] <- round(median(G62_H$Pct_Macrophages, na.rm = TRUE), digits = 1)
sum_G62[40, 1] <- round(quantile(G62_H$Pct_Macrophages, na.rm = TRUE, 0.25), digits = 1)
sum_G62[41, 1] <- round(quantile(G62_H$Pct_Macrophages, na.rm = TRUE, 0.75), digits = 1)

sum_G62[38, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$Pct_Macrophages), ] == TRUE), digits = 0)
sum_G62[39, 2] <- round(median(G62_MA$Pct_Macrophages, na.rm = TRUE), digits = 1)
sum_G62[40, 2] <- round(quantile(G62_MA$Pct_Macrophages, na.rm = TRUE, 0.25), digits = 1)
sum_G62[41, 2] <- round(quantile(G62_MA$Pct_Macrophages, na.rm = TRUE, 0.75), digits = 1)

sum_G62[38, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$Pct_Macrophages), ] == TRUE), digits = 0)
sum_G62[39, 3] <- round(median(G62_SA$Pct_Macrophages, na.rm = TRUE), digits = 1)
sum_G62[40, 3] <- round(quantile(G62_SA$Pct_Macrophages, na.rm = TRUE, 0.25), digits = 1)
sum_G62[41, 3] <- round(quantile(G62_SA$Pct_Macrophages, na.rm = TRUE, 0.75), digits = 1)

# Sputum lymphocytes (%)
sum_G62[42, 1] <- round(nrow(G62_H[complete.cases(G62_H$Pct_Lymphocytes), ] == TRUE), digits = 0)
sum_G62[43, 1] <- round(median(G62_H$Pct_Lymphocytes, na.rm = TRUE), digits = 1)
sum_G62[44, 1] <- round(quantile(G62_H$Pct_Lymphocytes, na.rm = TRUE, 0.25), digits = 1)
sum_G62[45, 1] <- round(quantile(G62_H$Pct_Lymphocytes, na.rm = TRUE, 0.75), digits = 1)

sum_G62[42, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$Pct_Lymphocytes), ] == TRUE), digits = 0)
sum_G62[43, 2] <- round(median(G62_MA$Pct_Lymphocytes, na.rm = TRUE), digits = 1)
sum_G62[44, 2] <- round(quantile(G62_MA$Pct_Lymphocytes, na.rm = TRUE, 0.25), digits = 1)
sum_G62[45, 2] <- round(quantile(G62_MA$Pct_Lymphocytes, na.rm = TRUE, 0.75), digits = 1)

sum_G62[42, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$Pct_Lymphocytes), ] == TRUE), digits = 0)
sum_G62[43, 3] <- round(median(G62_SA$Pct_Lymphocytes, na.rm = TRUE), digits = 1)
sum_G62[44, 3] <- round(quantile(G62_SA$Pct_Lymphocytes, na.rm = TRUE, 0.25), digits = 1)
sum_G62[45, 3] <- round(quantile(G62_SA$Pct_Lymphocytes, na.rm = TRUE, 0.75), digits = 1)

# Sputum mast cells (%)
sum_G62[46, 1] <- round(nrow(G62_H[complete.cases(G62_H$Pct_Mast_cells), ] == TRUE), digits = 0)
sum_G62[47, 1] <- round(median(G62_H$Pct_Mast_cells, na.rm = TRUE), digits = 1)
sum_G62[48, 1] <- round(quantile(G62_H$Pct_Mast_cells, na.rm = TRUE, 0.25), digits = 1)
sum_G62[49, 1] <- round(quantile(G62_H$Pct_Mast_cells, na.rm = TRUE, 0.75), digits = 1)

sum_G62[46, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$Pct_Mast_cells), ] == TRUE), digits = 0)
sum_G62[47, 2] <- round(median(G62_MA$Pct_Mast_cells, na.rm = TRUE), digits = 1)
sum_G62[48, 2] <- round(quantile(G62_MA$Pct_Mast_cells, na.rm = TRUE, 0.25), digits = 1)
sum_G62[49, 2] <- round(quantile(G62_MA$Pct_Mast_cells, na.rm = TRUE, 0.75), digits = 1)

sum_G62[46, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$Pct_Mast_cells), ] == TRUE), digits = 0)
sum_G62[47, 3] <- round(median(G62_SA$Pct_Mast_cells, na.rm = TRUE), digits = 1)
sum_G62[48, 3] <- round(quantile(G62_SA$Pct_Mast_cells, na.rm = TRUE, 0.25), digits = 1)
sum_G62[49, 3] <- round(quantile(G62_SA$Pct_Mast_cells, na.rm = TRUE, 0.75), digits = 1)

# FeNO (ppb)
sum_G62[50, 1] <- round(nrow(G62_H[complete.cases(G62_H$FeNO), ] == TRUE), digits = 0)
sum_G62[51, 1] <- round(median(G62_H$FeNO, na.rm = TRUE), digits = 0)
sum_G62[52, 1] <- round(quantile(G62_H$FeNO, na.rm = TRUE, 0.25), digits = 0)
sum_G62[53, 1] <- round(quantile(G62_H$FeNO, na.rm = TRUE, 0.75), digits = 0)

sum_G62[50, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$FeNO), ] == TRUE), digits = 0)
sum_G62[51, 2] <- round(median(G62_MA$FeNO, na.rm = TRUE), digits = 0)
sum_G62[52, 2] <- round(quantile(G62_MA$FeNO, na.rm = TRUE, 0.25), digits = 0)
sum_G62[53, 2] <- round(quantile(G62_MA$FeNO, na.rm = TRUE, 0.75), digits = 0)

sum_G62[50, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$FeNO), ] == TRUE), digits = 0)
sum_G62[51, 3] <- round(median(G62_SA$FeNO, na.rm = TRUE), digits = 0)
sum_G62[52, 3] <- round(quantile(G62_SA$FeNO, na.rm = TRUE, 0.25), digits = 0)
sum_G62[53, 3] <- round(quantile(G62_SA$FeNO, na.rm = TRUE, 0.75), digits = 0)

# FEV1 (%)
sum_G62[54, 1] <- round(nrow(G62_H[complete.cases(G62_H$FEV1_Pct_.L.), ] == TRUE), digits = 0)
sum_G62[55, 1] <- round(median(G62_H$FEV1_Pct_.L., na.rm = TRUE), digits = 1)
sum_G62[56, 1] <- round(quantile(G62_H$FEV1_Pct_.L., na.rm = TRUE, 0.25), digits = 1)
sum_G62[57, 1] <- round(quantile(G62_H$FEV1_Pct_.L., na.rm = TRUE, 0.75), digits = 1)

sum_G62[54, 2] <- round(nrow(G62_MA[complete.cases(G62_MA$FEV1_Pct_.L.), ] == TRUE), digits = 0)
sum_G62[55, 2] <- round(median(G62_MA$FEV1_Pct_.L., na.rm = TRUE), digits = 1)
sum_G62[56, 2] <- round(quantile(G62_MA$FEV1_Pct_.L., na.rm = TRUE, 0.25), digits = 1)
sum_G62[57, 2] <- round(quantile(G62_MA$FEV1_Pct_.L., na.rm = TRUE, 0.75), digits = 1)

sum_G62[54, 3] <- round(nrow(G62_SA[complete.cases(G62_SA$FEV1_Pct_.L.), ] == TRUE), digits = 0)
sum_G62[55, 3] <- round(median(G62_SA$FEV1_Pct_.L., na.rm = TRUE), digits = 1)
sum_G62[56, 3] <- round(quantile(G62_SA$FEV1_Pct_.L., na.rm = TRUE, 0.25), digits = 1)
sum_G62[57, 3] <- round(quantile(G62_SA$FEV1_Pct_.L., na.rm = TRUE, 0.75), digits = 1)

# Eczema
sum_G62[58, 1] <- nrow(subset(metadata_G62, Eczema_Diagnosed == "yes" & induced_sputum == "Healthy"))
sum_G62[59, 1] <- round((sum_G62[58, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[58, 2] <- nrow(subset(metadata_G62, Eczema_Diagnosed == "yes" & induced_sputum == "Moderate asthma"))
sum_G62[59, 2] <- round((sum_G62[58, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[58, 3] <- nrow(subset(metadata_G62, Eczema_Diagnosed == "yes" & induced_sputum == "Severe asthma"))
sum_G62[59, 3] <- round((sum_G62[58, 3]/sum_G62[1, 3])*100, digits = 1)

# Allergic Rhinitis
sum_G62[60, 1] <- nrow(subset(metadata_G62, Allergic_Rhinitis == "yes" & induced_sputum == "Healthy"))
sum_G62[61, 1] <- round((sum_G62[60, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[60, 2] <- nrow(subset(metadata_G62, Allergic_Rhinitis == "yes" & induced_sputum == "Moderate asthma"))
sum_G62[61, 2] <- round((sum_G62[60, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[60, 3] <- nrow(subset(metadata_G62, Allergic_Rhinitis == "yes" & induced_sputum == "Severe asthma"))
sum_G62[61, 3] <- round((sum_G62[60, 3]/sum_G62[1, 3])*100, digits = 1)

# Atopy
sum_G62[62, 1] <- nrow(subset(metadata_G62, Atopy == "positive" & induced_sputum == "Healthy"))
sum_G62[63, 1] <- round((sum_G62[62, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[62, 2] <- nrow(subset(metadata_G62, Atopy == "positive" & induced_sputum == "Moderate asthma"))
sum_G62[63, 2] <- round((sum_G62[62, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[62, 3] <- nrow(subset(metadata_G62, Atopy == "positive" & induced_sputum == "Severe asthma"))
sum_G62[63, 3] <- round((sum_G62[62, 3]/sum_G62[1, 3])*100, digits = 1)

# Sinusitis
sum_G62[64, 1] <- nrow(subset(metadata_G62, Sinusitis_Diagnosed == "yes" & induced_sputum == "Healthy"))
sum_G62[65, 1] <- round((sum_G62[64, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[64, 2] <- nrow(subset(metadata_G62, Sinusitis_Diagnosed == "yes" & induced_sputum == "Moderate asthma"))
sum_G62[65, 2] <- round((sum_G62[64, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[64, 3] <- nrow(subset(metadata_G62, Sinusitis_Diagnosed == "yes" & induced_sputum == "Severe asthma"))
sum_G62[65, 3] <- round((sum_G62[64, 3]/sum_G62[1, 3])*100, digits = 1)

# Nasal Polyps
sum_G62[66, 1] <- nrow(subset(metadata_G62, Nasal_Polyps_Diagnosed == "yes" & induced_sputum == "Healthy"))
sum_G62[67, 1] <- round((sum_G62[66, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[66, 2] <- nrow(subset(metadata_G62, Nasal_Polyps_Diagnosed == "yes" & induced_sputum == "Moderate asthma"))
sum_G62[67, 2] <- round((sum_G62[66, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[66, 3] <- nrow(subset(metadata_G62, Nasal_Polyps_Diagnosed == "yes" & induced_sputum == "Severe asthma"))
sum_G62[67, 3] <- round((sum_G62[66, 3]/sum_G62[1, 3])*100, digits = 1)

# Oral Corticosteroid Use
sum_G62[68, 1] <- nrow(subset(metadata_G62, oral_corticosteroids == "yes" & induced_sputum == "Healthy"))
sum_G62[69, 1] <- round((sum_G62[68, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[68, 2] <- nrow(subset(metadata_G62, oral_corticosteroids == "yes" & induced_sputum == "Moderate asthma"))
sum_G62[69, 2] <- round((sum_G62[68, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[68, 3] <- nrow(subset(metadata_G62, oral_corticosteroids == "yes" & induced_sputum == "Severe asthma"))
sum_G62[69, 3] <- round((sum_G62[68, 3]/sum_G62[1, 3])*100, digits = 1)

# Race: white caucasian
sum_G62[70, 1] <- nrow(subset(metadata_G62, Race == "race: white_caucasian" & induced_sputum == "Healthy"))
sum_G62[71, 1] <- round((sum_G62[70, 1]/sum_G62[1, 1])*100, digits = 1)

sum_G62[70, 2] <- nrow(subset(metadata_G62, Race == "race: white_caucasian" & induced_sputum == "Moderate asthma"))
sum_G62[71, 2] <- round((sum_G62[70, 2]/sum_G62[1, 2])*100, digits = 1)

sum_G62[70, 3] <- nrow(subset(metadata_G62, Race == "race: white_caucasian" & induced_sputum == "Severe asthma"))
sum_G62[71, 3] <- round((sum_G62[70, 3]/sum_G62[1, 3])*100, digits = 1)

