#!/bin/bash

#SBATCH -J diamond
#SBATCH -n 1
#SBATCH -t 00:15:00
#SBATCH --mem=8000

export OMP_NUM_THREADS=16
module load gcc/8.2.0
module load Python/2.7.14-anaconda-5.0.1-nsc1
module load R/3.5.1-nsc1-gcc-2018a

R --no-restore --no-save CMD BATCH "--args $1 $2 $3" diamond_sigma.R
