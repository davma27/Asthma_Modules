################################################################
# Method 3: DIAMOnD (Sigma, Modifier v1)                       #
################################################################

Args <- commandArgs(TRUE)
library(MODifieR, lib.loc = "~/R/x86_64-pc-linux-gnu-library/Modifier_v1")

input2 <- read.csv(paste(Args[1], sep = ""), sep = ",", dec = ".", row.names = 1, stringsAsFactors = FALSE)
output_genes <- list()
sample_name <- colnames(input2[as.numeric(Args[3])])
input2_df <- data.frame(GS = rownames(input2), P.Val = (-log10(input2[, as.numeric(Args[3])])), 
                        stringsAsFactors = FALSE)
input2_df <- input2_df[order(input2_df$P.Val, decreasing = TRUE), ]
dir.create(path = paste0("~/Documents/GSE83_v2/Modifier_v1/diamond_seeds/", sample_name, "/"), showWarnings = FALSE)
write.table(input2_df[1:200, 1], file = paste0("~/Documents/GSE83_v2/Modifier_v1/diamond_seeds/", sample_name, "/diamond_seed_genes.txt"), 
            col.names = FALSE, sep = "\t", dec = ".", row.names = FALSE, quote = FALSE)
module_sum <- diamond(network = paste0(Args[2]), input_genes = paste0("~/Documents/GSE83_v2/Modifier_v1/diamond_seeds/", sample_name, "/diamond_seed_genes.txt"), 
                              n_output_genes = 200,   # Maximum number of genes to include in the final module
                              seed_weight    = 10,    # Weight assignation for seed genes
                              include_seed   = TRUE)  # Boolean, if TRUE includes seed genes in the final module
output_genes <- module_sum$module_genes
write.table(output_genes, paste0("~/Documents/GSE83_v2/Modifier_v1/diamond_results/", sample_name, ".txt", sep = ""), 
            sep = "\t", row.names = FALSE, quote = FALSE)
            